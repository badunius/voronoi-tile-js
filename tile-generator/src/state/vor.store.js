/* jshint node: true */
/* jshint esversion: 6 */

import Vue from 'vue';
import Vuex from 'vuex';
import * as d3 from "d3";

import rnd from '../helpers/random.helper';

Vue.use(Vuex);

const store = new Vuex.Store({
    strict: true,
    state: {
        toolbarCaption: 'Tools:',
        canvasWidth: 640,
        canvasHeight: 640,
        gridSize: 30,
        drawSites: false,
        drawLinks: false,
        sites: [],
        innerSites: []
    },
    mutations: {
        updateCanvas: function(state, size) {
            if (size.hasOwnProperty('width')) {
                state.canvasWidth = size.width;
            }

            if (size.hasOwnProperty('height')) {
                state.canvasHeight = size.height;
            }
        },
        updateGrid(state, width) {
            if (width > 0) {
                state.gridSize = width;
            } else {
                console.warn(`VorStore >> updateGrid >> Invalid width: ${width}`);
            }
        },
        updateDrawSites(state, drawSites) {
            state.drawSites = drawSites;
        },
        updateDrawLinks(state, drawLinks) {
            state.drawLinks = drawLinks;
        },
        initSites(state, applyRandom) {
            const n = state.gridSize;
            const dx = state.canvasWidth / n;
            const halfDx = dx * 0.5;

            const dy = state.canvasHeight / n;
            const halfDy = dx * 0.5;

            const halfWidth = state.canvasWidth * 0.5;
            const halfHeight = state.canvasHeight * 0.5;

            let sites = d3.range(n).map(i => {
                let row = d3.range(n).map(j => {
                    return {
                        x: j * dx + halfDx - halfWidth,
                        y: i * dy + halfDy - halfHeight
                    };
                });

                return row;
            });

            console.info('VOR >> Regular sites generated: ', sites);

            if (applyRandom) {

                const cornerShift1 = {
                    x: rnd.getNormalRandom(-halfDx, +halfDx),
                    y: rnd.getNormalRandom(-halfDy, +halfDy),
                };

                const cornerShift2 = {
                    x: rnd.getNormalRandom(-halfDx, +halfDx),
                    y: rnd.getNormalRandom(-halfDy, +halfDy),
                };

                const cornerShift3 = {
                    x: rnd.getNormalRandom(-halfDx, +halfDx),
                    y: rnd.getNormalRandom(-halfDy, +halfDy),
                };

                const cornerShift4 = {
                    x: rnd.getNormalRandom(-halfDx, +halfDx),
                    y: rnd.getNormalRandom(-halfDy, +halfDy),
                };

                // Corner shifts
                // Green
                sites[0][0].x += cornerShift1.x;
                sites[0][n - 2].x += cornerShift1.x;
                sites[n - 2][0].x += cornerShift1.x;
                sites[n - 2][n - 2].x += cornerShift1.x;

                sites[0][0].y += cornerShift1.y;
                sites[0][n - 2].y += cornerShift1.y;
                sites[n - 2][0].y += cornerShift1.y;
                sites[n - 2][n - 2].y += cornerShift1.y;

                // Yellow
                sites[0][1].x += cornerShift2.x;
                sites[0][n - 1].x += cornerShift2.x;
                sites[n - 2][1].x += cornerShift2.x;
                sites[n - 2][n - 1].x += cornerShift2.x;

                sites[0][1].y += cornerShift2.y;
                sites[0][n - 1].y += cornerShift2.y;
                sites[n - 2][1].y += cornerShift2.y;
                sites[n - 2][n - 1].y += cornerShift2.y;

                // Red
                sites[1][0].x += cornerShift3.x;
                sites[1][n - 2].x += cornerShift3.x;
                sites[n - 1][0].x += cornerShift3.x;
                sites[n - 1][n - 2].x += cornerShift3.x;

                sites[1][0].y += cornerShift3.y;
                sites[1][n - 2].y += cornerShift3.y;
                sites[n - 1][0].y += cornerShift3.y;
                sites[n - 1][n - 2].y += cornerShift3.y;

                // Blue
                sites[1][1].x += cornerShift4.x;
                sites[1][n - 1].x += cornerShift4.x;
                sites[n - 1][1].x += cornerShift4.x;
                sites[n - 1][n - 1].x += cornerShift4.x;

                sites[1][1].y += cornerShift4.y;
                sites[1][n - 1].y += cornerShift4.y;
                sites[n - 1][1].y += cornerShift4.y;
                sites[n - 1][n - 1].y += cornerShift4.y;

                for (let i = 2; i < n - 2; i++) {
                    let edgeShift1 = {
                        x: rnd.getNormalRandom(-halfDx, +halfDx),
                        y: rnd.getNormalRandom(-halfDy, +halfDy),
                    };
                    let edgeShift2 = {
                        x: rnd.getNormalRandom(-halfDx, +halfDx),
                        y: rnd.getNormalRandom(-halfDy, +halfDy),
                    };

                    // Random for top and bottom edges
                    sites[0][i].x += edgeShift1.x;
                    sites[n - 2][i].x += edgeShift1.x;
                    sites[0][i].y += edgeShift1.y;
                    sites[n - 2][i].y += edgeShift1.y;

                    sites[1][i].x += edgeShift2.x;
                    sites[n - 1][i].x += edgeShift2.x;
                    sites[1][i].y += edgeShift2.y;
                    sites[n - 1][i].y += edgeShift2.y;

                    edgeShift1 = {
                        x: rnd.getNormalRandom(-halfDx, +halfDx),
                        y: rnd.getNormalRandom(-halfDy, +halfDy),
                    };

                    edgeShift2 = {
                        x: rnd.getNormalRandom(-halfDx, +halfDx),
                        y: rnd.getNormalRandom(-halfDy, +halfDy),
                    };

                    // Random for left and right edges
                    sites[i][1].x += edgeShift1.x;
                    sites[i][n - 1].x += edgeShift1.x;
                    sites[i][1].y += edgeShift1.y;
                    sites[i][n - 1].y += edgeShift1.y;
                }

                for (let i = 2; i < n - 2; i++) {
                    for (let j = 2; j < n - 2; j++) {
                        let innerShift = {
                            x: rnd.getNormalRandom(-halfDx, +halfDx),
                            y: rnd.getNormalRandom(-halfDy, +halfDy),
                        };

                        // Random for inner site
                        sites[i][j].x += innerShift.x;
                        sites[i][j].y += innerShift.y;
                    }
                }
            }

            state.sites = sites;
        }
    }
});

export default store;